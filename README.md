# prallel:matrix

The matrix keyword enables you to run the same job with multiple different variable sets in parallel.
See https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix .

HINT: The number of permutations cannot exceed 50. So 50 Jobs at max can be executed this way.
